# ONLINE STORE



![Microservises scheme](https://a.radikal.ru/a32/2004/75/1642ecda39cb.png)

## List of services
* User service - the user service is responsible for being the authentication gateway for the online store application. 
* Account service - storing the account information of a user.
* Inventory service - the inventory and product catalogs that are ordered on the online store.
* Shopping Cart Service - managing the products that a user has chosen to add to their online shopping cart.
* Order Service - ordering of products that a user has purchased.
* Payment Service - payment for goods ordered.

1. The main microservices used:  
Online-Store Web, User 
2. Auxiliary microservices:  
Account, Inventory, Order, Shopping-cart, Payment

## The main use cases:

1. User Authentication  
  User should be authenticated in the application before using it. The user can update his personal data. Application administrators also have the option to remove a user from the database.\
Services Used: User, Account

2. Selection products from the catalog  
  User has the opportunity to select products from the catalog available in the application. Whenever a user interacts the catalog, special application services ensure the relevance of information on the products contained in the catalog.\
  Services Used: Catalog, Inventory

3. Making an order  
  The user makes an order for the selected products. Special application services check the availability of chosen products in the store and, if successful, bill the user at the specified address.  Services Used: Order, Shopping-cart

4. Payment for goods  
  The user pays for the ordered products according to the invoice issued to him.\
  Services Used: Payment

## Team: 
* Akhmedzianova Adelina
* Antonov Nikolai
* Bokarev Ilia
* Fadeeva Ekaterina
* Latypova Dina
* Mavrina Regina
